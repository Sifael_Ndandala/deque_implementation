""" 
Implementation of the Deque Data Structure (Double Ended Queue)

In the class, we implement the following methods:

1. Check if queue is empty
2. Return the size of the queue
3. Enqueue to queue both ends
4. Dequeue from queue both ends
5. Return items at the top of queue both ends

"""


class Deque:

    def __init__(self):
        self.deque = [] 

    def is_empty(self):
        return self.deque == []

    def size(self):
        if self.is_empty():
            return None
        else:
            return len(self.deque)

    def enqueue_head(self, value):
        self.deque.insert(0, value)

    def enqueue_tail(self, value):
        self.deque.append(value)

    def dequeue_head(self):
        self.deque.pop(0)

    def dequeue_tail(self):
        self.deque.pop()

    
    def top_head(self):
        return self.deque[0]

    def top_tail(self):
        return self.deque[-1]

    def __str__(self):
        return " <-> ".join([str(x) for x in self.deque])



# Sample Implementation
my_deque = Deque()
print(my_deque.is_empty(), my_deque.size())

#  Adding Items 
my_deque.enqueue_head('Python')
my_deque.enqueue_head('C++')
my_deque.enqueue_tail('Java')
my_deque.enqueue_tail('JavaScript')

print(my_deque)

# Dequeue
my_deque.dequeue_head()
print(my_deque)

my_deque.dequeue_tail()
print(my_deque)

print(my_deque.is_empty(), my_deque.size())
